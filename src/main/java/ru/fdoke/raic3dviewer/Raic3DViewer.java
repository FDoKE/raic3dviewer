package ru.fdoke.raic3dviewer;

import com.jme3.app.SimpleApplication;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.debug.Arrow;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Line;
import com.jme3.scene.shape.Sphere;
import com.jme3.shadow.DirectionalLightShadowRenderer;
import com.jme3.util.TangentBinormalGenerator;
import ru.fdoke.raic3dviewer.model.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @author FDoKE
 */
public class Raic3DViewer extends SimpleApplication implements ActionListener {
    private static final String TO_REALTIME_ACTION = "realtime";
    private static final String PAUSE_RESUME_ACTION = "pauseResume";
    private static final String NEXT_STEP = "right";
    private static final String PREV_STEP = "left";

    private static final Vector3f WORLD_UP_VECTOR = new Vector3f(0, 1, 0);

    private static final Sphere sphereMesh = new Sphere(32, 32, 0.5f);
    private static final Box boxMesh = new Box(1, 1, 1);

    private final ConcurrentHashMap<Integer, ArrayList<Object>> geometriesByTick = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<Integer, String> debugByTick = new ConcurrentHashMap<>();
    private final ArrayList<Geometry> drawnGeometries = new ArrayList<>();

    private final ArrayList<Geometry> spheresPool = new ArrayList<>();
    private final ArrayList<Geometry> boxesPool = new ArrayList<>();
    private int spheres = 0;
    private int boxes = 0;

    private final ConcurrentLinkedQueue<Object> receivedGeometries = new ConcurrentLinkedQueue<>();

    public boolean realTime = true;
    public boolean playing = true;

    public int lastLoadedTick = 0;
    public int drawedTick = 0;
    public int receivingTick = 0;

    public float rawCurrentTick = 0;
    public float ticksPerSecond = 100;

    private BitmapText debugText;
    private BitmapText tickText;
    private RewindAction rewindAction = RewindAction.NO_ACTION;


    public Raic3DViewer() {
        showSettings = false;
    }

    @Override
    public void simpleInitApp() {
        // settings
        setPauseOnLostFocus(false);

        cam.setFrustumFar(100000);

        //input
        inputManager.addMapping(TO_REALTIME_ACTION, new KeyTrigger(KeyInput.KEY_UP));
        inputManager.addMapping(PAUSE_RESUME_ACTION, new KeyTrigger(KeyInput.KEY_DOWN));
        inputManager.addMapping(NEXT_STEP, new KeyTrigger(KeyInput.KEY_RIGHT));
        inputManager.addMapping(PREV_STEP, new KeyTrigger(KeyInput.KEY_LEFT));
        inputManager.addListener(this, TO_REALTIME_ACTION, PAUSE_RESUME_ACTION, NEXT_STEP, PREV_STEP);

        // camera
        cam.setLocation(new Vector3f(0.015041917f, 0.4572918f, 20));
        cam.lookAt(new Vector3f(0, 0, 0), WORLD_UP_VECTOR);

        flyCam.setMoveSpeed(25);
        flyCam.setDragToRotate(true);

        // fonts
        tickText = new BitmapText(guiFont, false);
        tickText.setSize(guiFont.getCharSet().getRenderedSize());
        tickText.setColor(ColorRGBA.Blue);
        guiNode.attachChild(tickText);

        debugText = new BitmapText(guiFont, false);
        debugText.setSize(guiFont.getCharSet().getRenderedSize());
        debugText.setColor(ColorRGBA.Blue);
        guiNode.attachChild(debugText);

        //lighting
        DirectionalLight light1 = new DirectionalLight();
        light1.setDirection(new Vector3f(0, -1, 1).normalizeLocal());
        light1.setColor(ColorRGBA.White.mult(2f));
        rootNode.addLight(light1);

        DirectionalLight light2 = new DirectionalLight();
        light2.setDirection(new Vector3f(1, 1, 1).normalizeLocal());
        light2.setColor(ColorRGBA.White.mult(0.5f));
        rootNode.addLight(light2);

        DirectionalLight light3 = new DirectionalLight();
        light3.setDirection(new Vector3f(-1, 1, -1).normalizeLocal());
        light3.setColor(ColorRGBA.White.mult(0.5f));
        rootNode.addLight(light3);
        rootNode.setShadowMode(RenderQueue.ShadowMode.Off);

        //shadow
        final int SHADOWMAP_SIZE = 512;
        DirectionalLightShadowRenderer dlsr = new DirectionalLightShadowRenderer(assetManager, SHADOWMAP_SIZE, 4);
        dlsr.setLight(light1);
        viewPort.addProcessor(dlsr);

        //enable if u need shiny graphics xD
        /*SSAOFilter ssaoFilter = new SSAOFilter(12.94f, 43.92f, 0.33f, 0.61f);
        FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
        fpp.addFilter(ssaoFilter);
        viewPort.addProcessor(fpp);*/
    }

    @Override
    public void simpleUpdate(float tpf) {
        synchronized (this) {
            if (rewindAction == RewindAction.BACKWARD) {
                manualTickChange(-tpf * ticksPerSecond);
            } else if (rewindAction == RewindAction.FORWARD) {
                manualTickChange(tpf * ticksPerSecond);
            }

            if (playing) {
                if (realTime) {
                    rawCurrentTick = lastLoadedTick;
                } else {
                    rawCurrentTick += tpf * ticksPerSecond;
                    if (rawCurrentTick > lastLoadedTick) {
                        rawCurrentTick = lastLoadedTick;
                    }
                }
            }

            int currentTick = (int) Math.floor(rawCurrentTick);

            tickText.setLocalTranslation(settings.getWidth() - tickText.getLineWidth(), tickText.getLineHeight(), 0);
            if (drawedTick != currentTick && geometriesByTick.get(currentTick) != null) {
                drawedTick = currentTick;

                for (Geometry geometry : drawnGeometries) {
                    geometry.removeFromParent();
                    spheres = 0;
                    boxes = 0;
                }
                drawnGeometries.clear();

                for (Object drawCommand : geometriesByTick.get(drawedTick)) {
                    Geometry geometry = null;
                    if (drawCommand instanceof DDrawArrow) {
                        DDrawArrow arrowParams = (DDrawArrow) drawCommand;
                        geometry = drawArrow(arrowParams.from, arrowParams.to, arrowParams.color);
                    } else if (drawCommand instanceof DDrawBox) {
                        DDrawBox boxParams = (DDrawBox) drawCommand;
                        geometry = drawBox(boxParams.name, boxParams.position, boxParams.size, boxParams.color);
                    } else if (drawCommand instanceof DDrawLine) {
                        DDrawLine lineParams = (DDrawLine) drawCommand;
                        geometry = drawLine(lineParams.from, lineParams.to, lineParams.color);
                    } else if (drawCommand instanceof DDrawSphere) {
                        DDrawSphere sphereParams = (DDrawSphere) drawCommand;
                        geometry = drawSphere(sphereParams.name, sphereParams.position, sphereParams.size, sphereParams.color);
                    }

                    if (geometry != null) {
                        rootNode.attachChild(geometry);
                        drawnGeometries.add(geometry);
                    }
                }

                String debug = debugByTick.get(drawedTick);
                if (debug == null) {
                    debug = "No Debug";
                }
                debugText.setText(debug);
            }

            tickText.setText(String.format("%d/%d/%d/%s", drawedTick, currentTick, receivingTick, playing && realTime ? "realtime" : (playing ? "playing" : "pause")));
            tickText.setLocalTranslation(settings.getWidth() - tickText.getLineWidth(), tickText.getLineHeight(), 0);
            debugText.setLocalTranslation(0, settings.getHeight(), 0);
        }
    }

    @Override
    public void simpleRender(RenderManager rm) {
    }

    public Geometry drawBox(String name, DVector3f position, DVector3f size, Color color) {
        Geometry box;
        if (boxes < boxesPool.size()) {
            box = boxesPool.get(boxes++);
        } else {
            box = createGeometry(name, boxMesh, true);
            boxesPool.add(box);
            boxes++;
        }

        createGeometry(name, boxMesh, true);
        box.setShadowMode(RenderQueue.ShadowMode.Receive);
        Material material = box.getMaterial();
        box.setLocalScale(size.x, size.y, size.z);
        box.setLocalTranslation(position.x, position.y, position.z);
        material.setColor("Diffuse", new ColorRGBA(color.getRed() / 255.f, color.getGreen() / 255.f, color.getBlue() / 255.f, color.getAlpha() / 255.f));
        return box;
    }

    public Geometry drawSphere(String name, DVector3f position, float size, Color color) {
        Geometry sphere;
        if (spheres < spheresPool.size()) {
            sphere = spheresPool.get(spheres++);
        } else {
            sphere = createGeometry(name, sphereMesh, true);
            spheresPool.add(sphere);
            spheres++;
        }

        sphere.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);
        Material material = sphere.getMaterial();
        sphere.setLocalScale(size);
        sphere.setLocalTranslation(position.x, position.y, position.z);
        material.setColor("Diffuse", new ColorRGBA(color.getRed() / 255.f, color.getGreen() / 255.f, color.getBlue() / 255.f, color.getAlpha() / 255.f));
        return sphere;
    }

    public Geometry drawArrow(DVector3f from, DVector3f to, Color color) {
        Arrow arrowMesh = new Arrow(new Vector3f(to.x - from.x, to.y - from.y, to.z - from.z));
        Geometry arrow = createSimpleGeometry("arrow", arrowMesh);
        arrow.setShadowMode(RenderQueue.ShadowMode.Off);
        arrow.setLocalTranslation(from.x, from.y, from.z);
        Material material = arrow.getMaterial();
        material.setColor("Color", new ColorRGBA(color.getRed() / 255.f, color.getGreen() / 255.f, color.getBlue() / 255.f, color.getAlpha() / 255.f));
        return arrow;
    }

    public Geometry drawLine(DVector3f from, DVector3f to, Color color) {
        Line lineMesh = new Line(new Vector3f(from.x, from.y, from.z), new Vector3f(to.x, to.y, to.z));
        Geometry line = createSimpleGeometry("line", lineMesh);
        line.setShadowMode(RenderQueue.ShadowMode.Off);
        Material material = line.getMaterial();
        material.setColor("Color", new ColorRGBA(color.getRed() / 255.f, color.getGreen() / 255.f, color.getBlue() / 255.f, color.getAlpha() / 255.f));
        return line;
    }

    private Geometry createGeometry(String name, Mesh mesh, boolean needTangents) {
        Geometry geometry = new Geometry(name, mesh.clone());
        Material material = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        material.setFloat("Shininess", 25);
        material.setBoolean("UseMaterialColors", true);
        material.setColor("Ambient", ColorRGBA.Black);
        material.setColor("Diffuse", ColorRGBA.Gray);
        material.setColor("Specular", ColorRGBA.Gray);
        geometry.setMaterial(material);
        if (needTangents) {
            TangentBinormalGenerator.generate(geometry.getMesh(), true);
        }
        return geometry;
    }

    private Geometry createSimpleGeometry(String name, Mesh mesh) {
        Geometry geometry = new Geometry(name, mesh.clone());
        Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        geometry.setMaterial(material);
        return geometry;
    }

    public synchronized void passInput(Object o) {
        if (o instanceof DStart) {
            receivingTick = ((DStart) o).tick;
        } else if (o instanceof DDrawArrow) {
            receivedGeometries.add(o);
        } else if (o instanceof DDrawBox) {
            receivedGeometries.add(o);
        } else if (o instanceof DDrawLine) {
            receivedGeometries.add(o);
        } else if (o instanceof DDrawSphere) {
            receivedGeometries.add(o);
        } else if (o instanceof DDebug) {
            String s = debugByTick.get(receivingTick);
            if (s == null) {
                s = ((DDebug) o).debugString;
            } else {
                s = s + "\n" + ((DDebug) o).debugString;
            }
            debugByTick.put(receivingTick, s);
        } else if (o instanceof DEnd) {
            synchronized (this) {
                ArrayList<Object> currentDraw = new ArrayList<>(receivedGeometries);
                geometriesByTick.put(receivingTick, currentDraw);
                receivedGeometries.clear();
                lastLoadedTick = receivingTick;
            }
        }
    }

    @Override
    public void onAction(String name, boolean isPressed, float tpf) {
        switch (name) {
            case TO_REALTIME_ACTION:
                if (isPressed) {
                    toRealtimeOrSimplePlay();
                }
                break;
            case PAUSE_RESUME_ACTION:
                if (isPressed) {
                    pauseResume();
                }
                break;
            case PREV_STEP:
                if (isPressed) {
                    rewindAction = RewindAction.BACKWARD;
                } else {
                    rewindAction = RewindAction.NO_ACTION;
                }
                break;
            case NEXT_STEP:
                if (isPressed) {
                    rewindAction = RewindAction.FORWARD;
                } else {
                    rewindAction = RewindAction.NO_ACTION;
                }
                break;
            default:
                break;
        }
    }

    public synchronized void manualTickChange(float delta) {
        playing = false;
        realTime = false;
        rawCurrentTick += delta;
        if (rawCurrentTick < 0) {
            rawCurrentTick = 0;
        } else if (rawCurrentTick > lastLoadedTick) {
            rawCurrentTick = lastLoadedTick;
        }
    }

    public synchronized void manualTickChangeTo(int tick) {
        playing = false;
        realTime = false;
        rawCurrentTick = tick;
        if (rawCurrentTick < 0) {
            rawCurrentTick = 0;
        } else if (rawCurrentTick > lastLoadedTick) {
            rawCurrentTick = lastLoadedTick;
        }
    }

    public synchronized void pauseResume() {
        rewindAction = RewindAction.NO_ACTION;
        if (playing) {
            playing = false;
            realTime = false;
        } else {
            playing = true;
            realTime = false;
        }
    }

    public synchronized void toRealtimeOrSimplePlay() {
        rewindAction = RewindAction.NO_ACTION;
        if (!playing || !realTime) {
            playing = true;
            realTime = true;
            rawCurrentTick = lastLoadedTick;
        } else {
            realTime = false;
        }
    }

    private enum RewindAction {
        FORWARD,
        BACKWARD,
        NO_ACTION
    }
}