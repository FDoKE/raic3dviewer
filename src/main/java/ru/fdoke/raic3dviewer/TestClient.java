package ru.fdoke.raic3dviewer;

import ru.fdoke.raic3dviewer.model.*;

import java.awt.*;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * @author FDoKE
 * <p>
 * Test client for showing interaction with the server on any client.
 */
public class TestClient {
    private static int tick = 0;

    private TestClient() {

    }

    public static void main(String[] args) throws IOException, InterruptedException {
        Socket socket = new Socket("127.0.0.1", 7271);

        ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
        while (true) {
            testSend(objectOutputStream); //for hot swap purposes
            Thread.sleep((long) (1000 / 100));
        }
    }

    private static void testSend(ObjectOutputStream objectOutputStream) throws IOException {
        float x = (float) (2 * Math.cos(tick / 20.f));
        float z = (float) (2 * Math.sin(tick / 20.f));
        float y = 0;


        // put start marker
        objectOutputStream.writeObject(new DStart(tick++));

        // put debug info again
        objectOutputStream.writeObject(new DDebug("Started tick!"));

        // put draw info
        objectOutputStream.writeObject(new DDrawBox("box",
                new DVector3f(0, 0, 0),
                1, 1, 1, Color.white
        ));

        objectOutputStream.writeObject(new DDrawBox("fromToBox",
                new DVector3f(-5, -2, 5),
                new DVector3f(5, -3, -5), Color.yellow));

        objectOutputStream.writeObject(new DDrawSphere("id1",
                new DVector3f(x, y, z), 1,
                new Color(255, 255, 255)));

        objectOutputStream.writeObject(new DDrawLine(new DVector3f(0, 0, 0), new DVector3f(0, 2, 0), Color.GREEN));
        objectOutputStream.writeObject(new DDrawLine(new DVector3f(0, 0, 0), new DVector3f(2, 0, 0), Color.RED));
        objectOutputStream.writeObject(new DDrawLine(new DVector3f(0, 0, 0), new DVector3f(0, 0, 2), Color.BLUE));

        // put debug info again and it will be concatenated
        objectOutputStream.writeObject(new DDebug("Ended tick!" + Math.random()));

        // put end marker
        objectOutputStream.writeObject(new DEnd());

        // u can flush anytime between DStart/DEnd
        objectOutputStream.flush();
    }
}
