package ru.fdoke.raic3dviewer;

import com.jme3.system.AppSettings;
import ru.fdoke.raic3dviewer.control.ControlForm;
import ru.fdoke.raic3dviewer.model.DEnd;
import ru.fdoke.raic3dviewer.model.DSync;

import javax.swing.event.MouseInputAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

/**
 * @author FDoKE
 * <p>
 * Entry point for Server
 */
public class ServerStart {

    private ServerStart() {
    }

    public static void main(String[] args) {
        final Raic3DViewer app = new Raic3DViewer();
        AppSettings settings = new AppSettings(true);
        settings.setTitle("Raic3DViewer");
        settings.setResolution(640, 480);
        settings.setFrameRate(120);
        app.setDisplayFps(false);
        app.setDisplayStatView(false);
        app.setSettings(settings);
        app.start();

        ControlForm controlForm = new ControlForm();
        controlForm.init(new MouseInputAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                app.manualTickChange(-1);
            }
        }, new MouseInputAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                app.manualTickChange(1);
            }
        }, new MouseInputAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                app.pauseResume();
            }
        }, new MouseInputAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                app.toRealtimeOrSimplePlay();
            }
        }, e -> {
            if (controlForm.kostil) return;
            app.manualTickChangeTo(controlForm.tickSlider.getValue());
        }, e -> {
            app.ticksPerSecond = controlForm.tpsSlider.getValue();
            controlForm.tpsSlider.setToolTipText(Float.toString(app.ticksPerSecond));
        }, () -> {
            while (true) {
                if (app.playing) {
                    controlForm.playPause.setText("Pause");
                } else {
                    controlForm.playPause.setText("Play");
                }
                if (app.playing && app.realTime) {
                    controlForm.realTime.setText("tps time");
                } else {
                    controlForm.realTime.setText("realtime");
                }
                controlForm.tickLabel.setText(app.drawedTick + "/" + app.lastLoadedTick);
                controlForm.kostil = true;
                controlForm.tickSlider.setMaximum(app.lastLoadedTick);
                controlForm.tickSlider.setValue(app.drawedTick);
                controlForm.kostil = false;

                try {
                    Thread.sleep(5);
                } catch (Exception ignored) {
                }
            }
        });

        Thread serverThread = new Thread(() -> {
            ServerSocket socket = null;
            try {
                socket = new ServerSocket();
                socket.bind(new InetSocketAddress("127.0.0.1", 7271));
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(111);
            }

            while (true) {
                try {
                    Socket client = socket.accept();
                    ObjectInputStream objectInputStream = new ObjectInputStream(client.getInputStream());
                    ObjectOutputStream objectOutputStream = new ObjectOutputStream(client.getOutputStream());
                    while (true) {
                        Object o = objectInputStream.readObject();
                        if (o instanceof DSync) {
                            // send sync only if current frame is last loaded tick
                            while (app.drawedTick != app.lastLoadedTick) {
                                Thread.sleep(1);
                            }
                            objectOutputStream.writeObject(o);
                            objectOutputStream.flush();
                            continue;
                        }

                        app.passInput(o);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        serverThread.setDaemon(true);
        serverThread.start();
    }
}
