package ru.fdoke.raic3dviewer.model;

import java.awt.*;
import java.io.Serializable;

public class DDrawSphere implements Serializable {
    private static final long serialVersionUID = 8421873742981394212L;

    public String name;
    public DVector3f position;
    public float size;
    public Color color;

    public DDrawSphere(String name, DVector3f position, float radius, Color color) {
        this.name = name;
        this.position = position;
        this.size = radius * 2;
        this.color = color;
    }
}
