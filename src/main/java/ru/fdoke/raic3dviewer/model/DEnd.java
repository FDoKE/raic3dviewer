package ru.fdoke.raic3dviewer.model;

import java.io.Serializable;

/**
 * End for tick and trigger for rendering
 */
public class DEnd implements Serializable {
    private static final long serialVersionUID = -157463073590731222L;
}
