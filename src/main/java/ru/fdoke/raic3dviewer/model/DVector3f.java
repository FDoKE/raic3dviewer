package ru.fdoke.raic3dviewer.model;

import java.io.Serializable;

public class DVector3f implements Serializable {
    private static final long serialVersionUID = -3875411625347291815L;

    public float x, y, z;

    public DVector3f(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}
