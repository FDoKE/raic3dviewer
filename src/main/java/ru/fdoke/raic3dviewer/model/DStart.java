package ru.fdoke.raic3dviewer.model;

import java.io.Serializable;

/**
 * Start of a tick
 */
public class DStart implements Serializable {
    private static final long serialVersionUID = 6085148276337032802L;

    public int tick;

    public DStart(int tick) {
        this.tick = tick;
    }
}
