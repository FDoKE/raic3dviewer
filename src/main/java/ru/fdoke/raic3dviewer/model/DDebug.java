package ru.fdoke.raic3dviewer.model;

import java.io.Serializable;

public class DDebug implements Serializable {
    private static final long serialVersionUID = -7587291706507338320L;

    public String debugString;

    /**
     * Simple debug string. All debug strings concat in one frame by "\n" separator
     */
    public DDebug(String debugString) {
        this.debugString = debugString;
    }
}
