package ru.fdoke.raic3dviewer.model;

import java.awt.*;
import java.io.Serializable;

public class DDrawBox implements Serializable {
    private static final long serialVersionUID = 6584789165748916489L;

    public String name;
    public DVector3f position;
    public DVector3f size;
    public Color color;

    /**
     * Position changes from center of the box same as scaling
     *
     **/
    public DDrawBox(String name, DVector3f position, float sizeX, float sizeY, float sizeZ, Color color) {
        this.name = name;
        this.position = position;
        // main reason of dividing by 2 is that default box width = 2,
        // in out case we think that sizeXYZ is a length of side, so we need to divide it by 2.
        this.size = new DVector3f(sizeX / 2, sizeY / 2, sizeZ / 2);
        this.color = color;
    }

    /**
     * Alternative position for the box by two vectors
     */
    public DDrawBox(String name, DVector3f from, DVector3f to, Color color) {
        this.name = name;

        float sizeX = to.x - from.x;
        float sizeY = to.y - from.y;
        float sizeZ = to.z - from.z;

        if (sizeX < 0) {
            from.x = from.x + sizeX;
            to.x = to.x - sizeX;
            sizeX = -sizeX;
        }

        if (sizeY < 0) {
            from.y = from.y + sizeY;
            to.y = to.y - sizeY;
            sizeY = -sizeY;
        }

        if (sizeZ < 0) {
            from.z = from.z + sizeZ;
            to.z = to.z - sizeZ;
            sizeZ = -sizeZ;
        }

        this.position = new DVector3f(from.x + sizeX / 2, from.y + sizeY / 2, from.z + sizeZ / 2);
        this.size = new DVector3f(sizeX / 2, sizeY / 2, sizeZ / 2);
        this.color = color;
    }
}
