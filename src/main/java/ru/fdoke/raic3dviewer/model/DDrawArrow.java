package ru.fdoke.raic3dviewer.model;

import java.awt.*;
import java.io.Serializable;

public class DDrawArrow implements Serializable {
    private static final long serialVersionUID = 9157874122567897456L;

    public DVector3f from;
    public DVector3f to;
    public Color color;

    /**
     * Simple from to arrow.
     */
    public DDrawArrow(DVector3f from, DVector3f to, Color color) {
        this.from = from;
        this.to = to;
        this.color = color;
    }

    /**
     * Arrow by direction.
     * @param dir must be normalized.
     */
    public DDrawArrow(DVector3f from, DVector3f dir, float size, Color color) {
        this.from = from;
        this.to = new DVector3f(from.x + dir.x * size, from.y + dir.y * size, from.z + dir.z * size);
        this.color = color;
    }
}
