package ru.fdoke.raic3dviewer.model;

import java.io.Serializable;

/**
 * Sync between client and server
 */
public class DSync implements Serializable {
    private static final long serialVersionUID = 5083376163377820835L;
}
